![OpenMensa](assets/logo.svg)

# OpenMensa for Ubuntu Touch

See what's for dinner at your university canteen or mensa.

- Download from the [OpenStore](https://open-store.io/app/umensa.neothethird) for Ubuntu Touch
- For apps for other OSs see [the OpenMensa website](openmensa.org/)
- Missing your canteen? [Request it](https://openmensa.org/c/new) or [add it yourself](https://openmensa.org/support)!
- Donate with [PayPal](https://paypal.me/neothethird) or [Liberapay](https://liberapay.com/neothethird)
- Contribute to [the OpenMensa project](https://openmensa.org/support)

## How to build

Set up [clickable](https://github.com/bhdouglass/clickable) and run `clickable` inside the working directory.

## Legal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
