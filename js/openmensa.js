// openmensa.js
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

function get_list(page, on_success, on_failure) {
    get("?page=" + page, on_success, on_failure);
}

function get_near(latitude, longitude, distance, on_success, on_failure) {
    get("?near[lat]=" + latitude + "&near[lng]=" + longitude + "&near[dist]=" + distance, on_success, on_failure);
}

function get_selected(ids, on_success, on_failure) {
    get("?ids=" + ids.join(","), on_success, on_failure);
}

function get_meals(id, date, on_success, on_failure) {
    get(id + "/days/" + date + "/meals/", on_success, on_failure);
}

function get_day(id, date, on_success, on_failure) {
    get(id + "/days/" + date + "/", on_success, on_failure);
}

function get_days(id, on_success, on_failure) {
    get(id + "/days/", on_success, on_failure);
}

function get(params, on_success, on_failure) {
    var req = new XMLHttpRequest();
    req.open("get", "http://openmensa.org/api/v2/canteens/" + params);
    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    req.onreadystatechange = function() {
        if(req.readyState === XMLHttpRequest.DONE) {
            var response = req.responseText;
            if(!response) {
                on_failure();
            } else if (response.toLowerCase().indexOf("bad") != 0) {
                on_success(JSON.parse(response));
            } else {
                on_failure(response);
            }
        }
    }

    req.send();
}
