// Map.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import QtPositioning 5.2

ApplicationWindow {
    id: mainWindow
    title: "OpenMensa"
    visible: true
    x: screen.desktopAvailableWidth / 4
    y: screen.desktopAvailableHeight / 5
    width: screen.desktopAvailableWidth / 2
    height: screen.desktopAvailableHeight / 1.5
    property variant favourites: []

    Settings {
        id: settings
        property int price: 0
        property int distance: 10
        property string favourites: "[]" // HACK: Work around QSettings being unable to process lists
    }

    Settings {
        category: "Cache"
        id: cache
        property var latitude
        property var longitude
    }

    Settings {
        category: "Window"
        property alias x: mainWindow.x
        property alias y: mainWindow.y
        property alias height: mainWindow.height
        property alias width: mainWindow.width
    }

    PageStack {
        id: stack
        anchors.fill: parent
    }

    PositionSource {
        id: geoposition
        active: true
        preferredPositioningMethods: PositionSource.AllPositioningMethods
        updateInterval: 10000
        onUpdateTimeout: {
            console.log("POSITION TIMEOUT")
            if (sourceError != PositionSource.NoError) console.log(sourceError)
        }
        onPositionChanged: {
            if (valid && geoposition.position.coordinate.latitude && geoposition.position.coordinate.longitude) {
                cache.latitude = geoposition.position.coordinate.latitude;
                cache.longitude = geoposition.position.coordinate.longitude;
            } else {
                active = false;
            }
        }
    }

    Component.onCompleted: {
        stack.push(Qt.resolvedUrl("Home.qml"));
        favourites = JSON.parse(settings.favourites) // HACK: Work around QSettings being unable to process lists
        i18n.domain = "openmensa.neothethird"
        i18n.bindtextdomain("openmensa.neothethird", i18nDirectory)
    }

    Component.onDestruction: {
        settings.favourites = JSON.stringify(favourites) // HACK: Work around QSettings being unable to process lists
    }
}
