// HomeAction.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3

MouseArea {
    property var action
    onClicked: action.trigger()
    Icon {
        id: icon
        height: parent.height * 0.75
        width: parent.with * 0.75
        anchors.centerIn: parent
        name: action.iconName
    }
    Label {
        id: label
        anchors.top: icon.bottom
        anchors.horizontalCenter: icon.horizontalCenter
        anchors.topMargin: parent.height * 0.05
        font.pixelSize: parent.height * 0.2
        text: action.text
    }
}
