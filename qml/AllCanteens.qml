// AllCanteens.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import "../js/openmensa.js" as OpenMensa
import "components"

Page {
    id: mainPage

    header: PageHeader {
        title: i18n.tr("All Canteens")
        flickable: scrollView.flickableItem

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr("Update")
                    onTriggered: scrollView.update()
                    iconName: "reload"
                }
            ]
        }
    }

    CanteenList {
        id: scrollView
        onUpdate: {
            loading = true;
            canteenList = []
            for (var i = 1; i < 100; i++) {
                OpenMensa.get_list(i,
                    function on_success(list) {
                        if (list != []) {
                            error = false;
                            canteenList = canteenList ? canteenList.concat(list) : list;
                        }
                    }, function on_failure() {
                        error = true;
                        errorText = i18n.tr("Can't reach OpenMensa API.")
                    }
                );
            }
            loading = false;
        }
    }
}
