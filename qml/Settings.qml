// Settings.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import "../js/openmensa.js" as OpenMensa

Page {
    id: aboutPage

    header: PageHeader {
        title: i18n.tr("Settings")
    }

    ScrollView {
        id: scrollView
        anchors {
            top: aboutPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }
        clip: true

        Column {
            id: aboutColumn
            spacing: units.gu(2)
            width: scrollView.width

            Rectangle {
                height: units.gu(6)
                width: parent.width
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("OpenMensa")
                fontSize: "x-large"
            }

            UbuntuShape {
                width: units.gu(24)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                radius: "medium"
                image: Image {
                    source: Qt.resolvedUrl("../assets/logo.png")
                }
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Version: ") + "0.1"
            }

            Rectangle {
                height: units.gu(6)
                width: parent.width
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Settings")
                fontSize: "x-large"
            }

            OptionSelector {
                text: i18n.tr("Price")
                selectedIndex: settings.price
                expanded: false
                model: [i18n.tr("Student"),
                        i18n.tr("Employee"),
                        i18n.tr("Pupil"),
                        i18n.tr("Other")]
                onDelegateClicked: settings.price = index
            }

            Column {
                width: parent.width
                Label {
                    horizontalAlignment: Text.AlignLeft
                    text: i18n.tr("Distance")
                }
                Slider {
                    minimumValue: 1
                    maximumValue: 100
                    width: parent.width
                    value: settings.distance
                    stepSize: 1
                    live: true
                    onValueChanged: settings.distance = value;
                }
            }

            Rectangle {
                height: units.gu(6)
                width: parent.width
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("About")
                fontSize: "x-large"
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                //TRANSLATORS: Please make sure the URLs are correct
                text: i18n.tr("This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU General Public License</a> for more details.")
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: "<a href='https://gitlab.com/NeoTheThird/openmensa/tree/" + "0.1" + "'>" + i18n.tr("SOURCE") + "</a> | <a href='https://gitlab.com/NeoTheThird/openmensa/issues'>" + i18n.tr("ISSUES") + "</a> | <a href='https://paypal.me/neothethird'>" + i18n.tr("DONATE") + "</a>"
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                style: Font.Bold
                text: i18n.tr("Copyright") + " (c) 2018 Jan Sprinz <neo@neothethird.de>"
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                style: Font.Bold
                text: i18n.tr("This app uses data that is kindly provided by OpenMensa: An open database for university canteens, built and maintained by volunteer contributors and published under free licenses. If your canteen is not supported by the database yet, you can either request it from the community, or put in the work to add it to the database yourself: ") + "<a href='https://openmensa.org/contribute'>OpenMensa Project</a>"
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Rectangle {
                height: units.gu(6)
                width: parent.width
            }
        }
    }
}
