// MyCanteens.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import "../js/openmensa.js" as OpenMensa
import "components"

Page {
    id: mainPage

    header: PageHeader {
        title: i18n.tr("My Canteens")
        flickable: scrollView.flickableItem

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr("Update")
                    onTriggered: scrollView.update()
                    iconName: "reload"
                }
            ]
        }
    }

    CanteenList {
        id: scrollView
        onUpdate: {
            loading = true;
            canteenList = [];
            if (favourites.length == 0) {
                loading = false;
                error = true;
                errorText = i18n.tr("You don't have any saved canteens.")
            } else {
                OpenMensa.get_selected(favourites,
                    function on_success(list) {
                        if (list.length > 0) {
                            error = false;
                            canteenList = canteenList ? canteenList.concat(list) : list;
                        }
                    }, function on_failure() {
                        error = true;
                        errorText = l18n.tr("Can't reach OpenMensa API.")
                    }
                );
                loading = false;
            }
        }
    }
}
