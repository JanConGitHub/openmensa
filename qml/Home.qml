// Home.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import "components"
import "../js/openmensa.js" as OpenMensa

Page {
    id: mainPage

    header: PageHeader {
        title: i18n.tr("Welcome to OpenMensa!")
        // flickable: scrollView.flickableItem

        trailingActionBar {
            numberOfSlots: 0
            actions: [
                Action {
                    text: i18n.tr("All")
                    onTriggered: stack.push(Qt.resolvedUrl("AllCanteens.qml"))
                    iconName: "view-list-symbolic"
                },
                Action {
                    text: i18n.tr("Nearby")
                    onTriggered: stack.push(Qt.resolvedUrl("NearbyCanteens.qml"))
                    iconName: "location"
                },
                Action {
                    text: i18n.tr("My Canteens")
                    onTriggered: stack.push(Qt.resolvedUrl("MyCanteens.qml"))
                    iconName: "starred"
                },
                Action {
                    text: i18n.tr("Settings")
                    onTriggered: stack.push(Qt.resolvedUrl("Settings.qml"))
                    iconName: "settings"
                },
                Action {
                    text: i18n.tr("Request")
                    onTriggered: Qt.openUrlExternally("http://openmensa.org/c/new")
                    iconName: "add"
                }
            ]
        }
    }

    ScrollView {
        id: scrollView
        anchors {
            top: mainPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        clip: true

        Column {
            id: aboutColumn
            spacing: units.gu(2)
            width: scrollView.width

            Rectangle {
                height: units.gu(6)
                width: parent.width
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("OpenMensa")
                fontSize: "x-large"
            }

            UbuntuShape {
                width: units.gu(24)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                radius: "medium"
                image: Image {
                    source: Qt.resolvedUrl("../assets/logo.png")
                }
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Version: ") + "0.1"
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("What's for dinner?")
            }

            Row {
                width: parent.width * 0.8
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: width * 0.2
                HomeAction {
                    height: parent.width * 0.4
                    width: height
                    action: Action {
                        text: i18n.tr("Nearby")
                        onTriggered: stack.push(Qt.resolvedUrl("NearbyCanteens.qml"))
                        iconName: "location"
                    }
                }
                HomeAction {
                    height: parent.width * 0.4
                    width: height
                    action: Action {
                        text: i18n.tr("My Canteens")
                        onTriggered: stack.push(Qt.resolvedUrl("MyCanteens.qml"))
                        iconName: "starred"
                    }
                }
            }

        }
    }
}
